
import java.io.PrintWriter;
/**
 * Class to describe a Cone 3d shape
 * 
 * @author Rick W. Phillips
 * @version July 1, 2016
 */
public class Cone extends ThreeDimensionalShape
{
    private double radius;
    private double height;
    
    /**
     * Constructor for objects of class Cone
     * 
     * @parm double     the radius if the Cone
     * @param double    the height of the Cone
     */
    public Cone(double radius, double height) 
    {
        this.radius = radius;
        this.height = height;
    }
    
    /**
     * Constructor for objects of class Cone
     * 
     * @parm Cone the Cone object to copy
     */
    public Cone(Cone c) 
    {
        radius = c.getRadius();
        height = c.getHeight();
    }
    
    /**
     * Get the radius of the Cone
     * 
     * @return double   the radius of the circle
     */
    public double getRadius() {
        return this.radius;
    }
    
    /**
     * Set the radius of the Cone
     * 
     * @param double    the new radius
     */
    public void setRadius(double radius) {
        this.radius = Math.abs(radius);
    }
    
    /**
     * Set the height Cone
     * 
     * @param double value to set height
     */
    public void setHeight(double height) {
        this.height = height;
    }
    
    /**
     * Get the height Cone
     * 
     * @return the height
     */
    public double getHeight() {
        return height;
    }
    
    /**
     * Get the surface area of the Cone
     * 
     * @return  area of the triangle as a double
     */
    public double getSurfaceArea() {
        
        return Math.PI * radius * (radius + Math.sqrt(radius * 2 + height * 2));
    }
    
    /**
     * Get the volume of the Cone
     * 
     * @return  perimeter of the circle as a double
     */
    public double getVolume() {
        return (Math.PI * radius * 2 * height) / 3;
    }
    
    /**
     * Method to print the content of the Cone to a file
     * 
     * @param PrintWriter   object for use in output of data
     */
    public void print(PrintWriter out) {
        out.printf("Radius:\t\t\t%.1f%n", getRadius());
        out.printf("Height:\t\t\t%.1f%n", getHeight());
        out.printf("Surface Area:\t%.2f%n", getSurfaceArea());
        out.printf("Volume:\t\t\t%.2f%n", getVolume());
    }
}
