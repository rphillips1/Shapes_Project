
import java.io.PrintWriter;
/**
 * Class to describe a Cube 3d shape
 * 
 * @author Rick W. Phillips
 * @version July 1, 2016
 */
public class Cube extends ThreeDimensionalShape
{
    private double length;
    
    /**
     * Constructor for the Cube
     *
     * @param double  length of the Cube
     */
    public Cube(double length) 
    {
        this.length = length;
    }
    
    /**
     * Constructor for the Cube
     *
     * @param Cube  the Cube to copy
     */
    public Cube(Cube c) 
    {
        length = c.getLength();
    }
    
    /**
     * Set the length
     * 
     * @param double    value to set length
     */
    public void setSideLength(double length) {
        this.length = length;
    }
    
    /**
     * Get the length
     * 
     * @return the length
     */
    public double getLength() {
        return length;
    }
    
    /**
     * Get the surface area of the Cube
     * 
     * @return  area of the Cube as a double
     */
    public double getSurfaceArea() {
        return 6 * length * length;
    }
    
    /**
     * Get the volume of the Cube
     * 
     * @return  perimeter of the circle as a double
     */
    public double getVolume() {
        return length * length * length;
    }
    
    /**
     * Method to print the content of the Cube to a file
     * 
     * @param PrintWriter   object for use in output of data
     */
    public void print(PrintWriter out) {
        out.printf("Side Length:\t%.1f%n", getLength());
        out.printf("Surface Area:\t%.2f%n", getSurfaceArea());
        out.printf("Volume:\t\t\t%.2f%n", getVolume());
    }
}
