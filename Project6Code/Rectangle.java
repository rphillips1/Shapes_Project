
import java.io.PrintWriter;
/**
 * Class to describe a Rectangle 2d shape
 * 
 * @author Rick W. Phillips
 * @version June 30, 2016
 */
public class Rectangle extends TwoDimensionalShape
{
    private double length;
    private double width;
    
    /**
     * Constructor for the Rectangle
     *
     * @param double  length of the Rectangle
     * @param double  width of the Rectangle
     */
    public Rectangle(double length, double width) 
    {
        this.length = length;
        this.width = width;
    }
    
    /**
     * Constructor for the Rectangle
     *
     * @param Rectangle the Rectangle to copy
     */
    public Rectangle(Rectangle r) 
    {
        length = r.getLength();
        width = r.getWidth();
    }
    
    /**
     * Set the length
     * 
     * @param double value to set length
     */
    public void setLength(double length) {
        this.length = length;
    }
    
    /**
     * Set the width
     * 
     * @param double value to set width
     */
    public void setWidth(double width) {
        this.width = width;
    }
    
    /**
     * Get the length
     * 
     * @return the length
     */
    public double getLength() {
        return length;
    }
    
    /**
     * Get the width
     * 
     * @return the width
     */
    public double getWidth() {
        return width;
    }
    
    /**
     * Get the area of the Rectangle
     * 
     * @return  area of the Rectangle
     */
    public double getArea() {
        
        return length * width;
    }
    
    /**
     * Get the perimeter of the Rectangle
     * 
     * @return  perimeter of the Rectangle
     */
    public double getPerimeter() {
        return length * 2 + width * 2;
    }
    
    /**
     * Method to print the content of the point to a file
     * see page 522 of our book
     * 
     * @param PrintWriter   object for use in output of data
     */
    public void print(PrintWriter out) {
        if(length == width) {
            out.println("This is a square.");
            out.printf("Side Length:\t%.1f%n", getWidth());
        }
        else {
            out.printf("Width:\t\t\t%.1f%n", getWidth());
            out.printf("Length:\t\t\t%.1f%n", getLength());
        }
        out.printf("Area:\t\t\t%.2f%n", getArea());
        out.printf("Perimeter:\t\t%.1f%n", getPerimeter());
    }
    
}
