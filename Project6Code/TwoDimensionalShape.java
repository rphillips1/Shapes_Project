
import java.io.PrintWriter;
/**
 * Abstract class TwoDimensionalShape - describes two dimensional shapes
 * 
 * @author Rick W. Phillips
 * @version July 1, 2016
 */
public abstract class TwoDimensionalShape implements Shape
{
    public abstract double getArea();
    public abstract double getPerimeter();
    public abstract void print(PrintWriter out);
}
