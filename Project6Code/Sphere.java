
import java.io.PrintWriter;
/**
 * Class to describe a Sphere 3d shape
 * 
 * @author Rick W. Phillips
 * @version July 1, 2016
 */
public class Sphere extends ThreeDimensionalShape
{
    private double radius;
    
    /**
     * Constructor for objects of class Sphere
     * 
     * @param double    the radius of the Sphere
     */
    public Sphere(double radius)
    {
        this.radius = Math.abs(radius);
    }

    /**
     * Constructor for objects of class Sphere
     * 
     * @parm Sphere the Sphere to copy
     */
    public Sphere(Sphere s)
    {
        this.radius = s.getRadius();
    }
    
    /**
     * Set the radius of the Sphere
     * 
     * @param double    the new radius
     */
    public void setRadius(double radius) {
        this.radius = Math.abs(radius);
    }
    
    /**
     * Get the radius of the Sphere
     * 
     * @return double   the radius of the circle
     */
    public double getRadius() {
        return this.radius;
    }
    
    /**
     * Get the surface area of the Sphere
     * 
     * @return  area of the triangle as a double
     */
    public double getSurfaceArea() {
        
        return 4 * Math.PI * radius * radius;
    }
    
    /**
     * Get the volume of the Sphere
     * 
     * @return  perimeter of the circle as a double
     */
    public double getVolume() {
        return 4 * Math.PI * radius * radius * radius / 3;
    }
    
    /**
     * Method to print the content of the Sphere to a file
     * 
     * 
     * @param PrintWriter   object for use in output of data
     */
    public void print(PrintWriter out) {
        out.printf("Radius:\t\t\t%.1f%n", getRadius());
        out.printf("Surface Area:\t%.2f%n", getSurfaceArea());
        out.printf("Volume:\t\t\t%.2f%n", getVolume());
    }
}
