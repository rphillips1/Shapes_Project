
import java.io.PrintWriter;
/**
 * Abstract class TwoDimensionalShape - describes three dimensional shapes
 * 
 * @author Rick W. Phillips
 * @version July 1, 2016
 */
public abstract class ThreeDimensionalShape implements Shape
{
    public abstract double getSurfaceArea();
    public abstract double getVolume();
    public abstract void print(PrintWriter out);
}
