
import java.io.PrintWriter;
/**
 * Class to describe a Cylinder 3d shape
 * 
 * @author Rick W. Phillips
 * @version July 1, 2016
 */
public class Cylinder extends ThreeDimensionalShape
{
    private double radius;
    private double height;
    
    /**
     * Constructor for objects of class Cylinder
     * 
     * @parm double     the radius of the Cylinder
     * @param double    the height of the Cylinder
     */
    public Cylinder(double radius, double height) 
    {
        this.radius = radius;
        this.height = height;
    }
    
    /**
     * Constructor for objects of class Cylinder
     * 
     * @parm Cylinder the Cylinder object to copy
     */
    public Cylinder(Cylinder c) 
    {
        radius = c.getRadius();
        height = c.getHeight();
    }
    
    /**
     * Get the radius of the Cylinder
     * 
     * @return double   the radius of the circle
     */
    public double getRadius() {
        return this.radius;
    }
    
    /**
     * Set the radius of the Cylinder
     * 
     * @param double    the new radius
     */
    public void setRadius(double radius) {
        this.radius = Math.abs(radius);
    }
    
    /**
     * Set the height Cylinder
     * 
     * @param double value to set height
     */
    public void setHeight(double height) {
        this.height = height;
    }
    
    /**
     * Get the height Cylinder
     * 
     * @return the height
     */
    public double getHeight() {
        return height;
    }
    
    /**
     * Get the surface area of the Cylinder
     * 
     * @return  area of the triangle as a double
     */
    public double getSurfaceArea() {
        
        return 2 * Math.PI * radius * height;
    }
    
    /**
     * Get the volume of the Cylinder
     * 
     * @return  perimeter of the circle as a double
     */
    public double getVolume() {
        return Math.PI * radius * radius * height;
    }
    
    /**
     * Method to print the content of the Cylinder to a file
     * 
     * @param PrintWriter   object for use in output of data
     */
    public void print(PrintWriter out) {
        out.printf("Radius:\t\t\t%.1f%n", getRadius());
        out.printf("Height:\t\t\t%.1f%n", getHeight());
        out.printf("Surface Area:\t%.2f%n", getSurfaceArea());
        out.printf("Volume:\t\t\t%.2f%n", getVolume());
    }
}
