
import java.io.PrintWriter;
/**
 * Class to describe a Triangle 2d shape
 * 
 * @author Rick W. Phillips
 * @version June 29, 2016
 */
public class Triangle extends TwoDimensionalShape
{
    private Point p1;
    private Point p2;
    private Point p3;
    
    /**
     * Constructor for the Triangle
     * 
     * @param Point the point to assign to p1
     * @param Point the point to assign to p2
     * @param Point the point to assign to p3
     */
    public Triangle(Point p1, Point p2, Point p3) 
    {
        this.p1 = new Point(p1);
        this.p2 = new Point(p2);
        this.p3 = new Point(p3);
    }
    
    /**
     * Constructor for the Triangle
     *
     * @param Trinagle  the Triangle to copy
     */
    public Triangle(Triangle t) 
    {
        this.p1 = new Point(t.p1);
        this.p2 = new Point(t.p2);
        this.p3 = new Point(t.p3);
    }
    
    /**
     * Set the p1 Point
     * 
     * @param Point point to set p1 to
     */
    public void setPoint1(Point p) {
        p1 = new Point(p);
    }
    
    /**
     * Set the p2 Point
     * 
     * @param Point point to set p2 to
     */
    public void setPoint2(Point p) {
        p2 = new Point(p);
    }
    
    /**
     * Set the p3 Point
     * 
     * @param Point point to set p3 to
     */
    public void setPoint3(Point p) {
        p3 = new Point(p);
    }
    
    /**
     * Get p1
     * 
     * @return  the Point object
     */
    public Point getPoint1() {
        return p1;
    }
    
    /**
     * Get p2
     * 
     * @return  the Point object
     */
    public Point getPoint2() {
        return p2;
    }
    
    /**
     * Get p2
     * 
     * @return  the Point object
     */
    public Point getPoint3() {
        return p3;
    }
    
    /**
     * Get the area of the Triangle
     * 
     * @return  area of the Triangle
     */
    public double getArea() {
        double p = getPerimeter() / 2;
        double s1 = p1.distance(p2);
        double s2 = p2.distance(p3);
        double s3 = p3.distance(p1);
        
        return Math.sqrt(p * (p - s1) * (p - s2) * (p - s3));
    }
    
    /**
     * Get the perimeter of the Triangle
     * 
     * @return  perimeter of the Triangle
     */
    public double getPerimeter() {
        return p1.distance(p2) + p2.distance(p3) + p3.distance(p1);
    }
    
    /**
     * Method to print the content of the Triangle to a file
     * 
     * @param PrintWriter   object for use in output of data
     */
    public void print(PrintWriter out) {
        out.println("The three end Points are:");
        out.println("[" + p1.getX() + ", " + p1.getY() + "]");
        out.println("[" + p2.getX() + ", " + p2.getY() + "]");
        out.println("[" + p3.getX() + ", " + p3.getY() + "]");
        out.printf("Area:\t\t\t%.2f%n", getArea());
        out.printf("Perimeter:\t\t%.1f%n", getPerimeter());
    }
    
    
}
