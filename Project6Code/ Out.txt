Triangle
The three end Points are:
[1.0, 1.0]
[5.0, 8.0]
[10.0, 4.0]
Area:			9.11
Perimeter:		13.8

Triangle
The three end Points are:
[2.0, 2.0]
[5.0, 8.0]
[12.0, 2.0]
Area:			9.00
Perimeter:		13.8

Triangle
The three end Points are:
[2.0, 2.0]
[2.0, 8.0]
[12.0, 2.0]
Area:			7.75
Perimeter:		13.6

Circle
Center:			[2.0, 2.0]
Radius:			4.0
Area:			50.27
Perimeter:		25.1

Circle
Center:			[2.0, 8.0]
Radius:			12.0
Area:			452.39
Perimeter:		75.4

Circle
Center:			[2.0, 8.0]
Radius:			5.0
Area:			78.54
Perimeter:		31.4

Rectangle
Width:			3.0
Length:			10.0
Area:			30.00
Perimeter:		26.0

Rectangle
Width:			4.0
Length:			10.0
Area:			40.00
Perimeter:		28.0

Rectangle
Width:			4.0
Length:			100.0
Area:			400.00
Perimeter:		208.0

Sphere
Radius:			4.0
Surface Area:	201.06
Volume:			268.08

Sphere
Radius:			12.0
Surface Area:	1809.56
Volume:			7238.23

Sphere
Radius:			5.0
Surface Area:	314.16
Volume:			523.60

Cube
Side Length:	4.0
Surface Area:	96.00
Volume:			64.00

Cube
Side Length:	12.0
Surface Area:	864.00
Volume:			1728.00

Cube
Side Length:	5.0
Surface Area:	150.00
Volume:			125.00

Cylinder
Radius:			4.0
Height:			10.0
Surface Area:	251.33
Volume:			502.65

Cylinder
Radius:			7.0
Height:			12.0
Surface Area:	527.79
Volume:			1847.26

Cylinder
Radius:			10.0
Height:			15.0
Surface Area:	942.48
Volume:			4712.39

Cone
Radius:			4.0
Height:			10.0
Surface Area:	116.76
Volume:			83.78

Cone
Radius:			7.0
Height:			12.0
Surface Area:	289.50
Volume:			175.93

Cone
Radius:			10.0
Height:			15.0
Surface Area:	536.30
Volume:			314.16

