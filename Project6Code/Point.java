
import java.io.PrintWriter;
/**
 * Class to describe a Point 2d object
 * 
 * @author Rick W. Phillips
 * @version June 26, 2016
 */
public class Point
{
    private double x;
    private double y;
    
    /**
     * Constructor for objects of class Point
     */
    public Point() {
        x = 0.0;
        y = 0.0;
    }
    
    /**
     * Constructor for objects of class Point with values
     * 
     * @param int   the x value of the point
     * @param int   the Y value of the point
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Copy constructor for objects of class Point.
     * 
     * @param Point Point object to clone
     */
    public Point(Point p) {
        x = p.getX();
        y = p.getY();
    }
    
    /**
     * Get method of the x value
     * 
     * @return  double value of x
     */
    public double getX() {
        return this.x;
    }
    
    /**
     * Set method of the x value
     * 
     * @param double value to set x
     */
    public void setX(double x) {
        this.x = x;
    }
    
    /**
     * Get method of the y value
     * 
     * @return  double value of y
     */
    public double getY() {
        return this.y;
    }
    
    /**
     * Set method of the y value
     * 
     * @param double value to set y
     */
    public void setY(double y) {
        this.y = y;
    }
    
    /**
     * Compute the distance between two points
     * 
     * @param Point the point to compare with
     * @return distance of the two points
     */
    public double distance(Point p) {
        
        double results =  Math.sqrt(2*(Math.abs(this.x - p.getX())) + 2*(Math.abs(this.y - p.getY())));
        return results;
    }
    
    /**
     * Method to print the content of the point to a file
     * see page 522 of our book
     * 
     * @param PrintWriter   object for use in output of data
     */
    public void print(PrintWriter out) {
        
    }
}
