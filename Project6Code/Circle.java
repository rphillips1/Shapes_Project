
import java.io.PrintWriter;
/**
 * Class to describe a Circle 2d shape
 * 
 * @author Rick W. Phillips
 * @version June 29, 2016
 */
public class Circle extends TwoDimensionalShape
{
    private double radius;
    private Point center;

    /**
     * Constructor for objects of class Circle
     * 
     * @parm Point      the center point of the circle
     * @param double    the radius of the circle
     */
    public Circle(Point p, double radius)
    {
        center = new Point(p);
        this.radius = Math.abs(radius);
    }

    /**
     * Constructor for objects of class Circle
     * 
     * @parm Circle      the Circle to copy
     */
    public Circle(Circle c)
    {
        this.center = new Point(c.center.getX(), c.center.getY());
        this.radius = c.getRadius();
    }

    /**
     * Get the radius
     * 
     * @return double   the radius of the Circle
     */
    public double getRadius() {
        return this.radius;
    }
    
    /**
     * Get the radius
     * 
     * @return double   the radius of the Circle
     */
    public Point getCenter() {
        return this.center;
    }
    
    /**
     * Set the radius
     * 
     * @param double    the new radius
     */
    public void setRadius(double radius) {
        this.radius = Math.abs(radius);
    }
    
    /**
     * Set the center
     * 
     * @param Point    the Point to use to set the center
     */
    public void setRadius(Point p) {
        this.center.setX(p.getX());
        this.center.setY(p.getY());
    }
    
    /**
     * Set the center of the circle
     * 
     * @param int   the x value to use to set the center
     * @param int   the y value to use to set the center
     */
    public void setRadius(int x, int y) {
        this.center.setX(x);
        this.center.setY(y);
    }
    
    /**
     * Get the area
     * 
     * @return  are of the Circle
     */
    public double getArea() {
        return Math.PI * radius * radius;
    }
    
    /**
     * Get the perimeter
     * 
     * @return  perimeter of the Circle
     */
    public double getPerimeter() {
        return Math.PI * 2 * radius;
    }
    
    /**
     * Method to print the content of the Circle to a file
     * 
     * @param PrintWriter   object for use in output of data
     */
    public void print(PrintWriter out) {
        out.println("Center:\t\t\t[" + center.getX() + ", " + center.getY() + "]");
        out.printf("Radius:\t\t\t%.1f%n", getRadius());
        out.printf("Area:\t\t\t%.2f%n", getArea());
        out.printf("Perimeter:\t\t%.1f%n", getPerimeter());
    }
}
