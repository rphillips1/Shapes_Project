
import java.io.PrintWriter;
/**
 * Shape inteface for providing minimum implementation rules
 * 
 * @author Rick W. Phillips
 * @version July 1, 2016
 */
public interface Shape
{
    void print(PrintWriter out);
}
