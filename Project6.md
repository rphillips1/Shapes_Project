### *CS-140 BL --- Summer I 2016*

# Project 5 - Shape Class Hierarchy

## Objectives
In this assignment you will create a hierarchy of classes related by inheritance.

## Background

You have been hired to design a set of classes that deal with mathematical shapes. The client has the following shapes in mind right now:

*     Circle - Circle is defined by its center (which is a point) and its radius
*     Rectangle - a closed polygon with 4 sides with right angles.
*     Triangle - a closed polygon with 3 points in the two-dimensional plane.
*     Cube - abstraction of a cube in three dimensional space. A cube is defined by its side length.
*     Cylinder - a shape in three-dimensional space - defined by its radius and height
*     Sphere - a shape in three-dimensional space - defined by its radius
*     Cone - a shape in three-dimensional space defined by its height and radius. 

Although these are the shapes the client has in mind right now, in the future they may add more shapes to the hierarchy. So you are supposed to design a system that is extensible (meaning that it is easy to add classes later).

We also have a Point class, which is not part of the hierarchy, but is used by classes in the hierarchy. After all, a Point is not really a shape!!

*    Point - abstraction of a point in the Cartesian coordinate system. A point is denoted by its x and y coordinates in the system. 

The interface minimally should have the following:

1.     A method that tells the client what the name of the shape is.
1.     A method that enables the client to print the shape in a nice format on the screen
1.     For two-dimensional shapes, the client wants operations that give the perimeter and area of each shape
1.     For three-dimensional shapes, the client wants operations that give the surface area and volume of each shape. 

This diagram represents the inheritance hierarchy that you are to implement:
<img src="Project6ClassDiagram.png">

## Specifications

### Point Class

A point is represented as a pair of Cartesian coordinates.

#### Instance Variables:

* x and y representing x and y coordinates (double)
	
#### Instance Methods:

*     A default constructor: sets the x and y coordinates to 0.0
*     Constructor that takes two doubles and initializes x and y
*     A copy constructor: takes a Point object as parameter and creates a new Point object that is an exact copy of the parameter.
*     get methods getX and getY
*     set methods setX and setY
*     distance method that returns the distance between this Point and another point. This method takes a Point object as a parameter and computes the length of the line segment between this Point and the parameter and returns the value as a double. The length of the line segment is defined as Math.sqrt( (this.x - p.x)2 + (this.y - p.y)2) where p is the parameter
*     print(PrintWriter out) method that that prints the point in the [x,y] format to the output file

### Circle Class

A circle is a TwoDimensionalShape and is represented by its radius and center (center is a Point object).

####    Instance Variables

*         radius (represented by a double) - radius cannot be negative
*         center which is a Point object

####    Instance Methods

*         Constructor - takes a Point object and a double and sets the center and radius
*         Copy constructor that takes a Circle and creates an exact copy (see Point class above)
*         Set methods for radius and center
*         Get methods for radius, center
*         Overridden methods getArea and getPerimeter
*             area (represented by a double) - Formula: Math.PI * radius * radius
*             perimeter (represented by a double) - Formula: 2 * Math.PI * radius
*         Overridden print(PrintWriter out) method that prints the circle object. See the output file for the format. 

### Triangle Class

A Triangle is a TwoDimensionalShape and is represented by its 3 corner points (which are all Point objects).

####    Instance Variables

*        3 Points -  p1, p2 and p3 (all Point objects)

####    Instance Methods

*         Constructor - takes 3 Point objects and initializes the three corner points
*         Copy constructor
*         Set methods for 3 corner points - setPoint1, setPoint2, setPoint3 - takes a Point object as parameter and sets the appropriate corner point
*         Get methods for 3 corner points - getPoint1, getPoint2 and getPoint3
*         Overridden methods getArea and getPerimeter
*         Overridden print(PrintWriter out) method that that prints the Triangle object. See the output file for the format.
*     See [http://www.mathopenref.com/heronsformula.html]() for formula on how to compute the area of a triangle given 3 sides.
*     The perimeter is the length of the 3 line segments - use the distance method from the Point class to compute the length of line segment between each pair of 3 corner points. 

### Rectangle Class

####    Instance Variables

*        length and width (both double) 

####    Instance Methods

*         Constructor - takes 2 doubles and initializes the length and width
*         Copy constructor
*         set and get methods for length and width
*         Overridden methods getArea and getPerimeter
*         Overridden print(PrintWriter out) method that prints a Rectangle object on the screen. See the output file for the format.
*         isSquare() method that returns true if both length and width are the same. 

### Cone Class

A Cone is a ThreeDimensionalShape and is represented by its radius and height

#### Instance Variables

*         radius (represented by a double)
*         height (represented by a double) 

####    Instance Methods

*         Constructor - takes radius and height
*         Copy constructor
*         Set methods for radius and height
*         Get methods for radius, height
*         Overridden methods getSurfaceArea and getVolume
*         Overridden print(PrintWriter out) method that prints a Cone object. See the output file for the format.
*         getVolume() - Formula (πr2h)/3 (r is the radius and h is the height)
*         getSurfaceArea() - Formula πr * (r + sqrt(r2 + h2))
*         [http://chemistry.about.com/od/chemistry101/ss/3dformulas_2.htm]() 

### Sphere Class

A Sphere is a ThreeDimensionalShape and is represented by its radius

*        radius (represented by a double); 

####    Instance Methods: similar to Cone and Cube

*        getSurfaceArea - Formula: 4 * PI * radius * radius
*        getVolume - Formula: 4 * PI * radius * radius * radius / 3 

### Cylinder Class

A Cylinder is a ThreeDimensionalShape and is represented by its radius and height

#### Instance Variables
*        radius (represented by a double);
*        height (represented by a double) 

####    Instance Methods: similar to Cone and Cube and Sphere

*        getSurfaceArea - Formula: 2 * PI * radius * height
*        getVolume - Formula: PI * radius * radius * height 

### Cube Class

A Cube is a ThreeDimensionalShape and is represented by its side length

#### Instance Variables

*        length (represented by a double); 

####    Instance Methods: similar to Cone
*        getSurfaceArea - Formula: 6 * length * length
*        getVolume - Formula: length * length * length 

### Test Class

You are given a Project6.java test class and an Out.txt output file. If your classes are designed correctly, your completed program should produce the same output as the one given to you.

## Note

I am expecting that you will have questions while working on this project. Often a client's specifications are not clear or completely thought out. A software developer must have a dialog with the client to clarify the specifications, or to present multiple options that the client must choose among. 
 	 
## This is an Individual Assignment - No Partners
As this is a Project (and not a Lab) you will be working on your own, not with a partner. You should not be sharing your code with anyone else, other than the instructor.  

You will need to fork your own private Project6 repository on GitLab for this project. The only person who should have any access to your repository is your instructor.  

You can ask questions on Piazza about setting up your repository on GitLab, about using Git to send code to the instructor, and general questions about how to write your code. However you should not be posting sections of code and asking others to find your errors. 

## Deliverables
* Be sure that you have javadocs for all classes and methods.
* Be sure that you have indented consistently.
 
The instructor will pull your Project6 from your GitLab repository to grade it. Make sure:
 
1. You have pushed all changes to your shared repository. (I can’t access local changes on your computer.)  
2. You have added your instructor as Master to your shared GitLab repository.  

### Due Date/Time
Your project must be pushed to GitLab by Friday, 1 July 2016 at 11:59pm. 
	
##Copyright and License
####&copy;2016 Karl R. Wurst and Aparna Mahadev, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.